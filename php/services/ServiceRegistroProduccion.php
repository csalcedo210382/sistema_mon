<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceRegistroProduccion extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function getListaOperarios(){
		$sql="	SELECT * FROM operario
				ORDER BY desOperario ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("desOperario"));
		return $res;
	}




}	
?>