<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceMaestro extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function saveRegistrosMaestro($data){
        $dato = $data->archivo;
        $caracteres = strlen($dato);
        $tabla = substr($dato, 0, ($caracteres - 5));;

        $responsable_grs = $this->getResultados("responsable","responsable_grs");

        $db_host = DB_HOST;
        $db_name = DB_NAME;
        $db_user = DB_USER;
        $db_pass = DB_PASS;
        include 'simplexlsx.class.php';
        try {
           $conn = new PDO( "mysql:host=$db_host;dbname=$db_name", "$db_user", "$db_pass");
           $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            echo $sql . "<br>" . $e->getMessage();
        }

        if($tabla == "maestro_grs"){

            $xlsx = new SimpleXLSX("../archivos_sistema/archivos_maestro/".$dato);
            $sql_truncate = "TRUNCATE TABLE ".$tabla;
            $this->db->query($sql_truncate);

            $stmt = $conn->prepare( "INSERT INTO ".$tabla." (id, created_date, header_text, status, type, po_number, order_line_number, item, supplier, order_qty, quantity, uom, item_number, price, currency, bill_of_lading_should_be_unique, sap_region_read_only, total, receiver, account, order_status, part_number, receipt_date, payment_term) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bindParam(1, $id);
            $stmt->bindParam(2, $created_date);
            $stmt->bindParam(3, $header_text);
            $stmt->bindParam(4, $status);
            $stmt->bindParam(5, $type);
            $stmt->bindParam(6, $po_number);
            $stmt->bindParam(7, $order_line_number);
            $stmt->bindParam(8, $item);
            $stmt->bindParam(9, $supplier);
            $stmt->bindParam(10, $order_qty);
            $stmt->bindParam(11, $quantity);
            $stmt->bindParam(12, $uom);
            $stmt->bindParam(13, $item_number);
            $stmt->bindParam(14, $price);
            $stmt->bindParam(15, $currency);
            $stmt->bindParam(16, $bill_of_lading_should_be_unique);
            $stmt->bindParam(17, $sap_region_read_only);
            $stmt->bindParam(18, $total);
            $stmt->bindParam(19, $receiver);
            $stmt->bindParam(20, $account);
            $stmt->bindParam(21, $order_status);
            $stmt->bindParam(22, $part_number);
            $stmt->bindParam(23, $receipt_date);
            $stmt->bindParam(24, $payment_term);

            foreach ($xlsx->rows() as $fields)
            {
                for ($i=0; $i < count($responsable_grs); $i++) {
                    if($fields[18] == $responsable_grs[$i]->responsable){
                        $id = $fields[0];
                        $created_date = $fields[1];
                        $header_text = $fields[2];
                        $status = $fields[3];
                        $type = $fields[4];
                        $po_number = $fields[5];
                        $order_line_number = $fields[6];
                        $item = $fields[7];
                        $supplier = $fields[8];
                        $order_qty = $fields[9];
                        $quantity = $fields[10];
                        $uom = $fields[11];
                        $item_number = $fields[12];
                        $price = $fields[13];
                        $currency = $fields[14];
                        $bill_of_lading_should_be_unique = $fields[15];
                        $sap_region_read_only = $fields[16];
                        $total = $fields[17];
                        $receiver = $fields[18];
                        $account = $fields[19];
                        $order_status = $fields[20];
                        $part_number = $fields[21];
                        $receipt_date = $fields[22];
                        $payment_term = $fields[23];
                        $stmt->execute();
                    }
                }
            }

        }

        if($tabla == "maestro_cierre"){

            $xlsx = new SimpleXLSX("../archivos_sistema/archivos_maestro/".$dato);
            $sql_truncate = "TRUNCATE TABLE ".$tabla;
            $this->db->query($sql_truncate);

            $stmt = $conn->prepare( "INSERT INTO ".$tabla." (project, wbs, description, budget_total, commit_total, actual_total, available_total, tt_actual_4, tt_actual_3, tt_actual_2, tt_actual_1) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bindParam(1, $project);
            $stmt->bindParam(2, $wbs);
            $stmt->bindParam(3, $description);
            $stmt->bindParam(4, $budget_total);
            $stmt->bindParam(5, $commit_total);
            $stmt->bindParam(6, $actual_total);
            $stmt->bindParam(7, $available_total);
            $stmt->bindParam(8, $tt_actual_4);
            $stmt->bindParam(9, $tt_actual_3);
            $stmt->bindParam(10, $tt_actual_2);
            $stmt->bindParam(11, $tt_actual_1);

            foreach ($xlsx->rows() as $fields)
            {
                //for ($i=0; $i < count($responsable_grs); $i++) {
                //    if($fields[18] == $responsable_grs[$i]->responsable){
                        $project = $fields[0];
                        $wbs = $fields[1];
                        $description = $fields[2];
                        $budget_total = $fields[3];
                        $commit_total = $fields[4];
                        $actual_total = $fields[5];
                        $available_total = $fields[6];
                        $tt_actual_4 = $fields[7];
                        $tt_actual_3 = $fields[8];
                        $tt_actual_2 = $fields[9];
                        $tt_actual_1 = $fields[10];
                        $stmt->execute();
                //    }
                //}
            }

        }

        if($tabla == "maestro_ops"){

            $xlsx = new SimpleXLSX("../archivos_sistema/archivos_maestro/".$dato);
            $sql_truncate = "TRUNCATE TABLE ".$tabla;
            $this->db->query($sql_truncate);

            $stmt = $conn->prepare( "INSERT INTO ".$tabla." (sap_error_message_header, chart_of_accounts, order_date_header, po_number_header, delivery_completed, version, requested_by_header, supplier2, supplier, line, item, qty, uom, price, line_total, currency, received, account, order_status_header, po_line_deletion_indicator, need_by, commodity, line_status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bindParam(1, $sap_error_message_header);
            $stmt->bindParam(2, $chart_of_accounts);
            $stmt->bindParam(3, $order_date_header);
            $stmt->bindParam(4, $po_number_header);
            $stmt->bindParam(5, $delivery_completed);
            $stmt->bindParam(6, $version);
            $stmt->bindParam(7, $requested_by_header);
            $stmt->bindParam(8, $supplier2);
            $stmt->bindParam(9, $supplier);
            $stmt->bindParam(10, $line);
            $stmt->bindParam(11, $item);
            $stmt->bindParam(12, $qty);
            $stmt->bindParam(13, $uom);
            $stmt->bindParam(14, $price);
            $stmt->bindParam(15, $line_total);
            $stmt->bindParam(16, $currency);
            $stmt->bindParam(17, $received);
            $stmt->bindParam(18, $account);
            $stmt->bindParam(19, $order_status_header);
            $stmt->bindParam(20, $po_line_deletion_indicator);
            $stmt->bindParam(21, $need_by);
            $stmt->bindParam(22, $commodity);
            $stmt->bindParam(23, $line_status);

            foreach ($xlsx->rows() as $fields)
            {
                for ($x=0; $x < count($responsable_grs); $x++) {
                    if($fields[6] == $responsable_grs[$x]->responsable){
                        $sap_error_message_header = $fields[0];
                        $chart_of_accounts = $fields[1];
                        $order_date_header = $fields[2];
                        $po_number_header = $fields[3];
                        $delivery_completed = $fields[4];
                        $version = $fields[5];
                        $requested_by_header = $fields[6];
                        $supplier2 = $fields[7];
                        $supplier = $fields[8];
                        $line = $fields[9];
                        $item = $fields[10];
                        $qty = $fields[11];
                        $uom = $fields[12];
                        $price = $fields[13];
                        $line_total = $fields[14];
                        $currency = $fields[15];
                        $received = $fields[16];
                        $account = $fields[17];
                        $order_status_header = $fields[18];
                        $po_line_deletion_indicator = $fields[19];
                        $need_by = $fields[20];
                        $commodity = $fields[21];
                        $line_status = $fields[22];
                        $stmt->execute();
                    }
                }
            }

        }

        return $tabla;

    }

    function listarArchivosMaestroPendientes(){

        $archivos = array();

        $directorio = opendir("../archivos_sistema/archivos_maestro"); //ruta actual
        while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
        {
            if (is_dir($archivo))//verificamos si es o no un directorio
            {
                //echo "[".$archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
            }
            else
            {

                $esArchivo = strpos($archivo, "log");

                if ($esArchivo === false) {

                    $bytes = filesize("../archivos_sistema/archivos_maestro/".$archivo);
                    $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
                    for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
                    $peso = ( round( $bytes, 2 ) . " " . $label[$i] );


                    $file = new stdClass();
                    $file->nombre = $archivo;
                    $file->log = "log_".$archivo;
                    $file->peso = $peso;
                    $file->fecha = date("Y-m-d", filectime("../archivos_sistema/archivos_maestro/".$archivo));

                    $archivos[] = $file;

                }
            }

            
        }

        return $archivos;

    }

    function eliminarArchivoMaestroPendiente($dato){
        unlink("../archivos_sistema/archivos_maestro/".$dato);
        unlink("../archivos_sistema/archivos_maestro/log_".$dato);
        return 1;

    }

    function getArchivoExiste($archivo){
        $nombre_fichero = "../archivos_sistema/archivos_maestro/".$archivo;
        $retorno = 0;
        if (file_exists($nombre_fichero)) {
            $retorno = 1;
        } else {
            $retorno = 0;
        }
        return $retorno;
    }



}	
?>