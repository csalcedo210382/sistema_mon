<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceUsuarios extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function getListaUsuarios(){
		$sql = "SELECT A.*, B.descripcionEstadoUsuario, C.descripcionTipoUsuario FROM usuario A LEFT JOIN estadousuario B
				ON A.estadoUsuario = B.idEstadoUsuario LEFT JOIN tipousuario C
				ON A.tipoUsuario = C.idTipoUsuario";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("tipoUsuario","claveUsuario","huellaUsuario","nombreUsuario","descripcionEstadoUsuario","descripcionTipoUsuario"));
		return $res;
	}

	function getListaEstados(){
		$sql = "SELECT * FROM estadousuario";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("descripcionEstadoUsuario"));
		return $res;
	}

	function getListaTipos(){
		$sql = "SELECT * FROM tipousuario";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("descripcionTipoUsuario"));
		return $res;
	}

	function saveFormularioUsuario($data){
		$procedimiento = $data->procedimiento;
		$idUsuario = $data->idUsuario;
		$dniUsuario = $data->dniUsuario;
		$nombreUsuario = $data->nombreUsuario;
		$fechaUsuario = $data->fechaUsuario;
		$tipoUsuario = $data->tipoUsuario;
		$estadoUsuario = $data->estadoUsuario;
		$claveUsuario = $data->claveUsuario;
		$usuario = $data->usuario;

		if($procedimiento == "GUARDAR"){
			$sql="INSERT INTO usuario (dniUsuario,nombreUsuario,tipoUsuario,claveUsuario,estadoUsuario,fechaUsuario,usuarioRegistrador)
				VALUES ('$dniUsuario',UPPER('$nombreUsuario'),'$tipoUsuario','$claveUsuario','$estadoUsuario','$fechaUsuario','$usuario')";

			$resNuevo=$this->db->query($sql);
		}else if($procedimiento == "MODIFICAR"){
			$sql="UPDATE usuario SET 
				dniUsuario = '$dniUsuario',
				nombreUsuario = UPPER('$nombreUsuario'),
				tipoUsuario = '$tipoUsuario',
				claveUsuario = '$claveUsuario',
				estadoUsuario = '$estadoUsuario',
				fechaUsuario = '$fechaUsuario',
				usuarioRegistrador = '$usuario'
				WHERE idUsuario = $idUsuario";

			$resEditado=$this->db->query($sql);
		}

		if($resNuevo){
			return 1;
		}elseif($resEditado){
			return 2;
		}else{
			return 0;
		}

	}

    function deleteFormularioUsuario($dato){
        $sql="DELETE FROM usuario WHERE idUsuario= $dato";
        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }

    function modificarMasivoFormularioUsuario($data){
    	$estado = $data->estado;
    	$usuarioRegistrador = $data->usuario;
    	$usuarios = $data->usuarios;

    	$listausuario = implode(",", $usuarios);

        $sql="UPDATE usuario SET estadoUsuario = '$estado', usuarioRegistrador='$usuarioRegistrador' WHERE idUsuario IN ($listausuario)";

        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }

    function eliminarMasivoFormularioUsuario($data){
    	$usuarioRegistrador = $data->usuario;
    	$usuarios = $data->usuarios;

    	$listausuario = implode(",", $usuarios);

        $sql="DELETE FROM usuario WHERE idUsuario IN ($listausuario)";

        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }










}	
?>