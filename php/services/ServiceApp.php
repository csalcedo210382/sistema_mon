<?php
require_once("ServiceTipoUsuario.php");

require_once("ServiceLogin.php");
require_once("ServiceUsuarios.php");

require_once("ServiceTienda.php");




require_once("ServiceAdministrativo.php");


require_once("ServiceMaestro.php");


require_once("Service.php");
class ServiceApp
{
	private $db;
	private $limiteServidor = 400;
	private $sqlPagina;
	private $idAdmin;
	private $direccion;
	function __construct(	) 
	{
		$GLOBALS['amfphp']['encoding'] = 'amf3';
		$this->db = new ezSQL_mysql(DB_USER,DB_PASS,DB_NAME,DB_HOST);		
		$this->idAdmin = 1;
		$this->direccion = "../../";
		$this->arrayServices = array("ServiceTipoUsuario","ServiceLogin","ServiceUsuarios","ServiceTienda","ServiceAdministrativo","ServiceMaestro");
	}
	public function ejecutar($funcion,$ar){
		$conexion;
		foreach ($this->arrayServices as $value) {
			$conexion = new $value();
			if( method_exists($conexion, $funcion) ){
				if(PRODUCTION_SERVER)
					$conexion->_iniciar();
				$res = call_user_func_array( array($conexion, $funcion),$ar);
			}
		}
		return $res;
	}	
}	
?>
