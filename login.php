<?php 
    error_reporting(0);
    session_start();
    require_once('php/config.php');
    require_once("php/services/ServiceLogin.php");
   
    $data[] = "";

    $logeado = $_SESSION["logeado"];

    if($logeado){
        $res = $_GET["r"];
        if($res == 1){
            $logeado = 0;
            $_SESSION["logeado"] = 0;
            $_SESSION["estadoUsuario"] = ""; 
            $_SESSION["dniUsuario"] = ""; 
            $_SESSION["nombreUsuario"] = "";
            $_SESSION["permisosUsuario"] = "";
            $_SESSION["tipoUsuario"] = "";
        }

    }else{ 
        $service = new ServiceLogin();
        $usuario = $_POST["usuario"];
        $clave = $_POST["clave"];

        $data = $service->validar($usuario,$clave);
        $logeado = $data[0]->idUsuario;

        if($logeado > 0){
            $_SESSION["logeado"] = $logeado;
            $_SESSION["estadoUsuario"] = $data[0]->estadoUsuario;  
            $_SESSION["dniUsuario"] = $_POST["usuario"];
            $_SESSION["nombreUsuario"] = $data[0]->nombreUsuario;
            $_SESSION["permisosUsuario"] = $data[0]->permisos;
            $_SESSION["tipoUsuario"] = $data[0]->descripcionTipoUsuario;
        }
        
    }
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SISTEMA INTEGRADO MONDELEZ</title>

    <!-- Bootstrap Core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="dist/css/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- ESTILO PERSONALIZADO UPC -->
    <link rel="stylesheet" type="text/css" href="dist/css/style_daca.css">

    <!-- ICONO DE PAGINA -->
    <link rel="icon" type="image/png" href="img/favicon.ico" />

    <!-- Custom Fonts -->
    <link href="dist/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="dist/css/alertify.core.css" />
    <link rel="stylesheet" href="dist/css/alertify.bootstrap.css" id="toggleCSS" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php 
    if($_SESSION["estadoUsuario"] == 1){ ?>
        <script type="text/javascript">
           sessionStorage.setItem("logeado",'<?=$_SESSION["logeado"]?>');
           sessionStorage.setItem("estadoUsuario",'<?=$_SESSION["estadoUsuario"]?>');
           sessionStorage.setItem("dniUsuario",'<?=$_SESSION["dniUsuario"]?>');
           sessionStorage.setItem("nombreUsuario",'<?=$_SESSION["nombreUsuario"]?>');
           sessionStorage.setItem("permisosUsuario",'<?=$_SESSION["permisosUsuario"]?>');
           sessionStorage.setItem("tipoUsuario",'<?=$_SESSION["tipoUsuario"]?>');
           window.open("index.php","_top");
        </script>
    <?php }  ?>

</head>

<body background="img/fondo_upc.png">

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-primary">
                    <div class="panel-heading text-center">
                        <h3 class="panel-title"> SISTEMA INTEGRADO MONDELEZ </h3>
                    </div>
                    <div class="panel-body login">
                        <form id="loginApp" action="login.php" method="post"> 
                            <fieldset>
                                <div class="input-group">
                                    <input type="usuario" name="usuario" placeholder="Usuario" class="form-control" autofocus>
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </div>
                                </div>
                                <br>
                                <div class="input-group">
                                    <input type="password" name="clave" placeholder="clave" class="form-control">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-lock"></span>
                                    </div>
                                </div>
                                <br>
                                <!--<div class="checkbox">
                                    <label>
                                        <input name="encuesta" type="checkbox" value="20">Encuesta Online
                                    </label>
                                </div>
                                 Change this to a button or input when using this as a form -->
                                <button type="button submit" class="btn btn-block btn-primary">Ingresar</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy" align="center">ENVÍE CONSULTAS O COMENTARIOS AL <a href="mailto:carlos.salcedo@igroupsac.com">WEBMASTER</a></div>
    </div>
    
    <script src="dist/js/alertify.min.js"></script>

    <!-- jQuery -->
    <script src="dist/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript 
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>-->

    <!-- Custom Theme JavaScript 
    <script src="dist/js/sb-admin-2.js"></script>-->

    <script>
        alertify.success("INGRESAR USUARIO Y CLAVE CORRECTOS");
    </script>

</body>

</html>
