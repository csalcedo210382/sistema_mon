<?php
	$db_host="localhost";
	$db_name="apoyo_daca";
	$db_user="root";
	$db_pass="";
    include 'simplexlsx.class.php';
    $xlsx = new SimpleXLSX( 'maestro_grs.xlsx' );
    try {
       $conn = new PDO( "mysql:host=$db_host;dbname=$db_name", "$db_user", "$db_pass");
       $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
        echo $sql . "<br>" . $e->getMessage();
    }
    $stmt = $conn->prepare( "INSERT INTO maestro_grs (id, created_date, header_text, status, type, po_number, order_line_number, item, supplier, order_qty, quantity, uom, item_number, price, currency, bill_of_lading_should_be_unique, sap_region_read_only, total, receiver, account, order_status, part_number, receipt_date, payment_term) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->bindParam(1, $id);
    $stmt->bindParam(2, $created_date);
    $stmt->bindParam(3, $header_text);
    $stmt->bindParam(4, $status);
    $stmt->bindParam(5, $type);
    $stmt->bindParam(6, $po_number);
    $stmt->bindParam(7, $order_line_number);
    $stmt->bindParam(8, $item);
    $stmt->bindParam(9, $supplier);
    $stmt->bindParam(10, $order_qty);
    $stmt->bindParam(11, $quantity);
    $stmt->bindParam(12, $uom);
    $stmt->bindParam(13, $item_number);
    $stmt->bindParam(14, $price);
    $stmt->bindParam(15, $currency);
    $stmt->bindParam(16, $bill_of_lading_should_be_unique);
    $stmt->bindParam(17, $sap_region_read_only);
    $stmt->bindParam(18, $total);
    $stmt->bindParam(19, $receiver);
    $stmt->bindParam(20, $account);
    $stmt->bindParam(21, $order_status);
    $stmt->bindParam(22, $part_number);
    $stmt->bindParam(23, $receipt_date);
    $stmt->bindParam(24, $payment_term);

    foreach ($xlsx->rows() as $fields)
    {
        if($fields[18] == "Claudia Sanchez"){
            $id = $fields[0];
            $created_date = $fields[1];
            $header_text = $fields[2];
            $status = $fields[3];
            $type = $fields[4];
            $po_number = $fields[5];
            $order_line_number = $fields[6];
            $item = $fields[7];
            $supplier = $fields[8];
            $order_qty = $fields[9];
            $quantity = $fields[10];
            $uom = $fields[11];
            $item_number = $fields[12];
            $price = $fields[13];
            $currency = $fields[14];
            $bill_of_lading_should_be_unique = $fields[15];
            $sap_region_read_only = $fields[16];
            $total = $fields[17];
            $receiver = $fields[18];
            $account = $fields[19];
            $order_status = $fields[20];
            $part_number = $fields[21];
            $receipt_date = $fields[22];
            $payment_term = $fields[23];
            $stmt->execute();
        }
    }
