
function cargoDocumento(){
	$('#cambiarContrasena').on('submit', function (e) {
	    //console.log("data",$('#cambiarContrasena').serializeArray());
	    var array = $('#cambiarContrasena').serializeArray();
	    validarEnviarDatos(array);
	    
	    return false;
	});
	var servicePrincipal;
	var botonCambiarContrasena = $("#botonCambiarContrasena");
	var btnMenuGuardarCambios = $("#btnMenuGuardarCambios");
	botonCambiarContrasena.on("click",clickCambiarContrasena);
	btnMenuGuardarCambios.on("click",actualizarDatos);
	botonCambiarContrasena.data("estadoActivo",false);

	var nombre;
	var apellido;
	var correo;
	var consultado;
	function clickCambiarContrasena(){
		var estadoActivo = botonCambiarContrasena.data("estadoActivo");
		if(estadoActivo){
			botonCambiarContrasena.html("Cambiar contraseña");
			botonCambiarContrasena.removeClass("btn-default");
			botonCambiarContrasena.addClass("btn-primary");		
			$("#cambiarContrasena").fadeOut("slow");			
		}
		else{
			botonCambiarContrasena.html("Salir Edición");			
			botonCambiarContrasena.removeClass("btn-primary");
			botonCambiarContrasena.addClass("btn-default");
			$("#cambiarContrasena").fadeIn("slow");
			
		}
		botonCambiarContrasena.data("estadoActivo",!estadoActivo);
	}
	function mostrarDatos(){
	//	console.log("mostrando Datos");
		if(servicePrincipal == null){
			servicePrincipal = new Service(CANVAS_URL_P+"webService/index.php");
		}
		consultado = true;
		servicePrincipal.procesar("getDatosUsuario",cargoDatosUsuario);
	}
	function cargoDatosUsuario(evt){
		consultado = false;
		//console.log("datos usuario",evt);
		nombre = evt.nombre;
		apellido = evt.apellido;
		correo = evt.correo;
		$("#lblMenuUsuario").html(evt.usuario);
		$("#lblMenuNombre").val(evt.nombre);
		$("#lblMenuApellido").val(evt.apellido);
		$("#lblMenuCorreo").val(evt.correo);
	}
	function actualizarDatos(){
		if(consultado){
			return;
		}
		var nuevoNombre = $("#lblMenuNombre").val();
		var nuevoApellido = $("#lblMenuApellido").val();
		var nuevoCorreo = $("#lblMenuCorreo").val();
		if(nombre != nuevoNombre || apellido != nuevoApellido || correo != nuevoCorreo){
			servicePrincipal.procesar("actualizarDatos",nuevoNombre,nuevoApellido,nuevoCorreo,resultadosDeActulizacion);
		}
	}
	function resultadosDeActulizacion(evt){
		if(evt == 0){
			alert("Error al actualizar información");
			return;
		}
		var nuevoNombre = $("#lblMenuNombre").val();
		var nuevoApellido = $("#lblMenuApellido").val();
		var nombreFinal = nuevoNombre +" "+nuevoApellido;
		$("#lblMenuNombreUsuario").html(nombreFinal);
	}
	$('#modalPerfil').on('show.bs.modal', function (e) {	  	
	  	mostrarDatos();

	});


	function validarEnviarDatos(array){
		console.log("cambiar clave",array);
		var clave1,clave2,clave3,dato,i;
		for ( i = 0; i < array.length; i++) {
			dato = array[i];
			if(dato.name == "contrasena1"){
				clave1 = dato.value;
			}
			else if(dato.name == "contrasena2"){
				clave2 = dato.value;
			}
			else if(dato.name == "contrasena3"){
				clave3 = dato.value;
			}
		};
		if(clave2 != clave3){
			alert("Las claves que se pidieron que sean iguales no lo son");
			return;
		}
		servicePrincipal.procesar("cambiarClave",clave1,clave2,clave3,resultadoCambioClave);
	}
	function resultadoCambioClave(evt){
		//console.log("resultado evt",evt);
		if(evt == 0){
			alert("La Contraseña ingresada es incorrecta");
			return;
		}
		alert("Se cambio la contraseña correctamente");
		$('#cambiarContrasena')[0].reset();
		clickCambiarContrasena();
	}
}
window.addEventListener("load",cargoDocumento);
