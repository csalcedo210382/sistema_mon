var service = new Service("webService/index.php");


$(function() 
{

iniciarControlTipoUsuario();

});

var arrayTipoUsuario = [];
var permisosTipoUsuario = "";
var listaModulos = [];
var modulosSeleccionados = "";
var seleccionados = [];
var resultado = [];

function iniciarControlTipoUsuario(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");
   



    $("#menu_tipo_usuario").on('click', function(){
      cargaConsultasIniciales();  
    })
    
    $("#boton_menu_tipo_usuario").on('click', function(){
      cargaConsultasIniciales();  
    })

    function cargaConsultasIniciales(){
        service.procesar("getListaTipoUsuario",cargaListaTipoUsuario);
        //service.procesar("getListaEstados",cargaListaEstados);
        //service.procesar("getListaTipos",cargaListaTipos);
        //cargarPeriodos();
    }


    function cargaListaTipoUsuario(evt){
        listadeTipoUsuario = evt;
        $("#tablaResultadoFormularioTipoUsuario tbody").html("");

        if ( listadeTipoUsuario == undefined ) return;

        for(var i=0; i<listadeTipoUsuario.length ; i++){

            var fila = $("<tr>");
            //var celdaBotones = $("<td>");
            datoRegistro = listadeTipoUsuario[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeTipoUsuario[i].descripcionTipoUsuario +'</td>');

            var contenedorBotonPermisos = $("<td>");

            var btnMostrar = $("<button class='btn btn-info btn-sm' title='Mostrar'>");
            btnMostrar.html('<span class="glyphicon glyphicon-eye-open"></span> MOSTRAR PERMISOS');
            btnMostrar.data("data",datoRegistro);
            btnMostrar.on("click",permisosFormularioTipoUsuario); 

            contenedorBotonPermisos.append(btnMostrar);
            fila.append(contenedorBotonPermisos);

            var contenedorSelectores = $("<td><div class='input-group-btn'>");

            var chkSeleccionar = $("<input type='checkbox' title='Seleccionar'>");
            //chkSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            chkSeleccionar.data("data",datoRegistro);
            chkSeleccionar.on("change",seleccionarFormularioTipoUsuario);

            contenedorSelectores.append(chkSeleccionar);

            fila.append(contenedorSelectores);


            var contenedorBotones = $("<td>");


            var btnSeleccionar = $("<button class='btn btn-primary btn-sm botonesTipoUsuario' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span> EDITAR');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarFormularioTipoUsuario);

            var btnEliminar = $("<button class='btn btn-danger btn-sm botonesTipoUsuario' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarFormularioTipoUsuario);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaResultadoFormularioTipoUsuario tbody").append(fila);

        }

    }

    function permisosFormularioTipoUsuario(){
        var data = $(this).data("data");
        $("#modal_tipo_usuario").modal('show');
        service.procesar("getListaModulos",function(evt){
            listaModulos = evt;
            modulosSeleccionados = data.permisos;
            mostrarModulosSeleccionados(listaModulos,modulosSeleccionados,data.idTipoUsuario);
        });

    }

    /*$("#cargarPermisosFormularioTipoUsuario").on('click', function(){
        $("#modal_tipo_usuario").modal('show');
        service.procesar("getListaModulos",function(evt){
            listaModulos = evt;
            modulosSeleccionados = permisosTipoUsuario;
            mostrarModulosSeleccionados(listaModulos,modulosSeleccionados);
        }); 
    })*/

    function mostrarModulosSeleccionados(arrayLista,cadenaSeleccionados,perfil){ 
        resultado = arrayLista;
        seleccionados = corregirNull(cadenaSeleccionados).split(",");
        var seleccionado = "";
        var valores = [];

        $("#modal_tipo_usuario table tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            valores = [];
            seleccionado = "";
            for(var x=0; x<seleccionados.length ; x++){
                if(resultado[i].idPerfil == seleccionados[x]){
                    seleccionado = "checked";
                }
            }

            var fila = $("<tr>");
            //var celdaBotones = $("<td>");
            
            datoRegistro = resultado[i];
            valores.push(resultado[i]);
            valores.push(perfil);
            valores.push(seleccionado);
            fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].titulo +'</td>');

            var contenedorSelectores = $("<td><div class='input-group-btn'>");

            var chkSeleccionar = $("<input type='checkbox' title='Seleccionar' "+seleccionado+">");
            //chkSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            chkSeleccionar.data("data",valores);
            chkSeleccionar.on("change",seleccionarModulo);

            contenedorSelectores.append(chkSeleccionar);

            fila.append(contenedorSelectores);


            $("#modal_tipo_usuario table tbody").append(fila);

        }  
    }

    function seleccionarModulo(){
        var data = $(this).data("data");
        var estado = data[2];
        var tipoUsuario = data[1]
        var modulo = data[0];
        
        if(estado == "checked"){
            removeItemFromArr ( seleccionados, modulo.idPerfil );
            alertify.error("MODULO : "+ modulo.titulo +" , QUITADO ");
            service.procesar("modificarModuloPerfil",seleccionados.toString(),tipoUsuario,resultadoModulosTipoUsuario);
        }else{
            seleccionados.push(modulo.idPerfil);
            alertify.success("MODULO : "+ modulo.titulo +" , AGREGADO ");
            service.procesar("modificarModuloPerfil",seleccionados.toString(),tipoUsuario,resultadoModulosTipoUsuario);
        }
    }

    function resultadoModulosTipoUsuario(evt){
        mostrarModulosSeleccionados(resultado,seleccionados.toString(),evt);
        service.procesar("getListaTipoUsuario",cargaListaTipoUsuario);
    }

    function seleccionarFormularioTipoUsuario(){
        var data = $(this).data("data");
        var idTipoUsuario = data.idTipoUsuario;
        var valor = $.inArray(idTipoUsuario, arrayTipoUsuario);

        if(valor >= 0){
        	removeItemFromArr( arrayTipoUsuario, idTipoUsuario );
        }else{
        	arrayTipoUsuario.push(idTipoUsuario);
        }
        
        var contar = arrayTipoUsuario.length;

        if(contar > 1){
        	$(".opcionestipousuario").css("display","none");
            $(".botonesTipoUsuario").prop( "disabled", true );
        	$(".opcionestipousuariomasivo").css("display","");
        }else{
        	$(".opcionestipousuario").css("display","");
            $(".botonesTipoUsuario").prop( "disabled", false );
        	$(".opcionestipousuariomasivo").css("display","none");
        }
    }

    function editarFormularioTipoUsuario(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DEL TIPO DE USUARIO : " + data.descripcionTipoUsuario +" ?", function (e) {
            if (e) {
                $("#codigoFormularioTipoUsuario").val(data.idTipoUsuario);
                $("#tipoUsuarioFormularioTipoUsuario").val(data.descripcionTipoUsuario);
                $("#nombreFormularioTienda").val(data.nombreTienda);
                permisosTipoUsuario = (data.permisos).split(',');

                $("#botonGuardarTipoUsuario").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    }



    function eliminarFormularioTipoUsuario(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR TIPO DE USUARIO : " + data.descripcionTipoUsuario +" ?", function (e) {
            if (e) {
                service.procesar("deleteFormularioTipoUsuario",data.idTipoUsuario,resultadoDeleteFormularioTipoUsuario);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteFormularioTipoUsuario(evt){
        service.procesar("getListaTipoUsuario",cargaListaTipoUsuario);
    }

    $("#botonGuardarTipoUsuario").on('click', function(){

        var procedimiento = $("#botonGuardarTipoUsuario").html();
        var idTipoUsuario = $("#codigoFormularioTipoUsuario").val();

        var tipoUsuario = $("#tipoUsuarioFormularioTipoUsuario").val();
        //var permisos  = permisosTipoUsuario.join(',');

        if(tipoUsuario != ""){

            alertify.confirm("¿ SEGURO DE GUARDAR EL REGISTRO ?", function (e) {
                if (e) {

                    var objetoFormulario = new Object()
                        objetoFormulario.procedimiento = procedimiento;
                        objetoFormulario.idTipoUsuario = idTipoUsuario;
                        objetoFormulario.tipoUsuario = tipoUsuario;
                        //objetoFormulario.permisos = permisos;

                    service.procesar("saveFormularioTipoUsuario",objetoFormulario,cargaFormularioTipoUsuario);

                    alertify.success("HA ACEPTADO GUARDAR EL REGISTRO");

                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });

        }else{
            alertify.error("COMPLETAR INFORMACION DEL FORMULARIO");
        }



    })

    $("#botonCancelarTipoUsuario").on('click',function(){
        alertify.confirm("¿ SEGURO DE CANCELAR EL PROCEDO ?", function (e) {
            if (e) {
		        alertify.success("PROCESO CANCELADO");
		        formularioTipoUsuarioEstandar();
            }
        });
    })


    $("#botonEliminarMasivoFormularioTipoUsuario").on('click',function(){
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS MASIVOS ?", function (e) {
            if (e) {

                var objetoFormulario = new Object()
                    objetoFormulario.tipos = arrayTipoUsuario;
                    objetoFormulario.usuario = usuario;

                service.procesar("eliminarMasivoFormularioTipoUsuario",objetoFormulario,cargaEliminarMasivoFormularioTipoUsuario);

                alertify.success("HA ACEPTADO ELIMINAR LOS REGISTROS");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    })

    function cargaEliminarMasivoFormularioTipoUsuario(evt){
    	if(evt == 1 ){
            alertify.success("REGISTROS ELIMINADOS SATISFACTORIAMENTE");
            $(".opcionestipousuario").css("display","");
        	$(".opcionestipousuariomasivo").css("display","none");
        	arrayTipoUsuario.length=0;
            service.procesar("getListaTipoUsuario",cargaListaTipoUsuario);
        }else{
        	alertify.error("PROCESO NO EJECUTADO");
        }
    }

    function cargaFormularioTipoUsuario(evt){
        if(evt == 1 ){
            alertify.success("REGISTRO GUARDADO SATISFACTORIAMENTE");
            service.procesar("getListaTipoUsuario",cargaListaTipoUsuario);
            formularioTipoUsuarioEstandar();
        }else if(evt == 2 ){
            alertify.success("REGISTRO MODIFICADO SATISFACTORIAMENTE");
            service.procesar("getListaTipoUsuario",cargaListaTipoUsuario);
            formularioTipoUsuarioEstandar();
        }else{
            alertify.error("REGISTRO NO GUARDADO"); 
        }
    }

    function formularioTipoUsuarioEstandar(){
        $("#tipoUsuarioFormularioTipoUsuario").val("");
        permisosTipoUsuario = [];
        $("#botonGuardarTienda").html("GUARDAR");
    }


    $("#botonListarImprimirTipoUsuario").on('click',function(){
        window.open("reportes/reporteTipoUsuario.php");
    })






    function corregirNull(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = "";
        }
        return resultado;
    }


    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

}


