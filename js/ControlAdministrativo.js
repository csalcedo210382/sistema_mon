var service = new Service("webService/index.php");

var cuestionario = Array;


$(function() 
{

iniciarControlAdministrativo();

});


function iniciarControlAdministrativo(){

	var dniUsuario = sessionStorage.getItem("dniUsuario");
	var nombreUsuario = sessionStorage.getItem("nombreUsuario");
	var tipoUsuario = sessionStorage.getItem("tipoUsuario");
	var codigo_usuario = sessionStorage.getItem("logeado");


	$("#usuario").html(nombreUsuario);
	$("#tipo").html(tipoUsuario);


	//LLENADO DE INFORMACION EN EL AREA DE AVANCE
	//service.procesar("getListaAreaRango",resultadoAvanceCapturaInicial);

	function resultadoAvanceCapturaInicial(evt){
        listadeAreaRango = corregirNullArreglo(evt.rangos);
        listaCapturas = corregirNullArreglo(evt.capturas);
        listaJustificados = corregirNullArreglo(evt.justificados);

        var cantidad = 0;
        var avance = 0;
        var porcentaje = 0;

        for(var i=0; i<listadeAreaRango.length ; i++){
            var filaInicio =parseFloat(listadeAreaRango[i].area_ini_ran);
            var filaFinal = parseFloat(listadeAreaRango[i].area_fin_ran);

            for (var z=filaInicio; z<=filaFinal; z++) {
            	cantidad++;
                for (var y=0; y<listaCapturas.length; y++) {
                    var valorArea = parseFloat(listaCapturas[y].area_cap);
                    if (z == valorArea){
                        avance++;
                    }
                }
                for (var y=0; y<listaJustificados.length; y++) {
                    var valorArea = parseFloat(listaJustificados[y].lote);
                    if (z == valorArea){
                        avance++;
                    }
                }
            }

        }

        porcentaje = ((parseFloat(avance) / parseFloat(cantidad))*100).toFixed(2);
        porcentaje = corregirNull(porcentaje);

		$(".progressAvanceCaptura").css('width', porcentaje+'%').attr('aria-valuenow', porcentaje);
		$(".progressAvanceCaptura").html(porcentaje+' %'); 		
	}


    $("#listarConflictoBarras").on('click',function(){
        service.procesar("listarConflictoBarras",resultadoListarConflictoBarras);
    })

    function resultadoListarConflictoBarras(evt){
        resultado = evt;
        $("#tablaConflictoBarras tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){

            var fila = $("<tr>");
            fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].area_cap +'</td><td>'+ resultado[i].barra_cap +'</td><td>'+ resultado[i].cant_cap +'</td>');
            $("#tablaConflictoBarras tbody").append(fila);

        }
    }

    function corregirNull(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = "";
        }
        return resultado;
    }
    
    function corregirNullArreglo(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = [];
        }
        return resultado;
    }

}


