var service = new Service("webService/index.php");


$(function() 
{

iniciarControlUsuario();

});

var arrayUsuarios = [];

function iniciarControlUsuario(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var hora = f.getHours();
    var minutos = f.getMinutes();
    var segundos = f.getSeconds();

    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia + " "+ hora + ":" + minutos + ":" + segundos;

    var usuario = sessionStorage.getItem("dniUsuario");


    $('#fechaFormularioUsuario').val(fecha);

    



    $("#menu_usuarios").on('click', function(){
      cargaConsultasIniciales();  
    })
    
    $("#boton_menu_usuarios").on('click', function(){
      cargaConsultasIniciales();  
    })

    function cargaConsultasIniciales(){
        service.procesar("getListaUsuarios",cargaListaUsuarios);
        service.procesar("getListaEstados",cargaListaEstados);
        service.procesar("getListaTipos",cargaListaTipos);
        //cargarPeriodos();
    }

    $("#botonActualizarUsuarios").on('click',function(){
        service.procesar("getListaUsuarios",cargaListaUsuarios);
    })
    
    function cargaListaEstados(evt){
        resultado = evt;
        $("#estadoFormularioUsuario").html("");
        $("#estadoMasivoFormularioUsuario").html("");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#estadoFormularioUsuario" ).append( "<option value='"+ resultado[i].idEstadoUsuario +"'>"+ resultado[i].descripcionEstadoUsuario +"</option>" );
        }
        for(var i=0; i<resultado.length ; i++){
            $("#estadoMasivoFormularioUsuario" ).append( "<option value='"+ resultado[i].idEstadoUsuario +"'>"+ resultado[i].descripcionEstadoUsuario +"</option>" );
        }
    } 

    function cargaListaTipos(evt){
        resultado = evt;
        $("#tipoFormularioUsuario").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#tipoFormularioUsuario" ).append( "<option value='"+ resultado[i].idTipoUsuario +"'>"+ resultado[i].descripcionTipoUsuario +"</option>" );
        }
    } 



    function cargaListaUsuarios(evt){
        //console.log(evt);
        var usuarioActivos = 0;
        listadeUsuarios = evt;
        $("#tablaResultadoFormularioUsuarios tbody").html("");
        $("#tablaResultadoFiltroFormularioUsuarios tbody").html("");

        if ( listadeUsuarios == undefined ) return;

        for(var i=0; i<listadeUsuarios.length ; i++){

            var fila = $("<tr>");
            //var celdaBotones = $("<td>");
            datoRegistro = listadeUsuarios[i];

            var huellaUsuario = "NO";

            if(listadeUsuarios[i].huellaUsuario != ""){ huellaUsuario = "SI"; }
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeUsuarios[i].dniUsuario +'</td><td>'+ listadeUsuarios[i].nombreUsuario +'</td><td>'+ listadeUsuarios[i].claveUsuario +'</td><td>'+ listadeUsuarios[i].descripcionTipoUsuario +'</td><td>'+ listadeUsuarios[i].descripcionEstadoUsuario +'</td><td>'+ listadeUsuarios[i].fechaUsuario +'</td>');

            var contenedorSelectores = $("<td><div class='input-group-btn'>");

            var chkSeleccionar = $("<input type='checkbox' title='Seleccionar'>");
            //chkSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            chkSeleccionar.data("data",datoRegistro);
            chkSeleccionar.on("change",seleccionarFormularioUsuario);

            contenedorSelectores.append(chkSeleccionar);

            fila.append(contenedorSelectores);


            var contenedorBotones = $("<td>");

            

            var btnSeleccionar = $("<button class='btn btn-primary btn-sm botonesUsuarios' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span> EDITAR');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarFormularioUsuario);

            var btnEliminar = $("<button class='btn btn-danger btn-sm botonesUsuarios' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarFormularioUsuario);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaResultadoFormularioUsuarios tbody").append(fila);

            if(listadeUsuarios[i].descripcionEstadoUsuario == "ACTIVO"){
                usuarioActivos = usuarioActivos + 1;
                $("#tablaResultadoFiltroFormularioUsuarios tbody").append('<tr><td>'+usuarioActivos+'</td><td>'+ listadeUsuarios[i].dniUsuario +'</td><td>'+ listadeUsuarios[i].nombreUsuario +'</td><td>'+ listadeUsuarios[i].descripcionTipoUsuario +'</td><td>'+ listadeUsuarios[i].descripcionEstadoUsuario +'</td></tr>');
            }

        }

    }

    function seleccionarFormularioUsuario(){
        var data = $(this).data("data");
        var idUsuario = data.idUsuario;
        var valor = $.inArray(idUsuario, arrayUsuarios);

        if(valor >= 0){
        	removeItemFromArr( arrayUsuarios, idUsuario );
        }else{
        	arrayUsuarios.push(idUsuario);
        }
        
        var contar = arrayUsuarios.length;

        if(contar > 1){
        	$(".opciones").css("display","none");
            $(".botonesUsuarios").prop( "disabled", true );
        	$(".opcionesmasivo").css("display","");
        }else{
        	$(".opciones").css("display","");
            $(".botonesUsuarios").prop( "disabled", false );
        	$(".opcionesmasivo").css("display","none");
        }
    }

    function editarFormularioUsuario(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DEL USUARIO : " + data.nombreUsuario +" ?", function (e) {
            if (e) {
                $("#dniFormularioUsuario").val(data.dniUsuario);
                $("#nombreFormularioUsuario").val(data.nombreUsuario);
                $("select#tipoFormularioUsuario").val(data.tipoUsuario);
                $("select#estadoFormularioUsuario").val(data.estadoUsuario);
                $("#fechaFormularioUsuario").val(data.fechaUsuario);
                $("#claveFormularioUsuario").val(data.claveUsuario);
                $("#codigoFormularioUsuario").val(data.idUsuario);

                $("#botonGuardarUsuarios").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    }

    function eliminarFormularioUsuario(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS DEL USUARIO : " + data.nombreUsuario +" ?", function (e) {
            if (e) {
                service.procesar("deleteFormularioUsuario",data.idUsuario,resultadoDeleteFormularioUsuario);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteFormularioUsuario(evt){
        service.procesar("getListaUsuarios",cargaListaUsuarios);
    }

    $("#botonGuardarUsuarios").on('click', function(){

        var procedimiento = $("#botonGuardarUsuarios").html();
        var idUsuario = $("#codigoFormularioUsuario").val();

        var dniUsuario = $("#dniFormularioUsuario").val();
        var nombreUsuario  = $("#nombreFormularioUsuario").val();
        var fechaUsuario = fecha;
        var tipoUsuario = $("select#tipoFormularioUsuario").val();
        var estadoUsuario = $("select#estadoFormularioUsuario").val();
        var claveUsuario = $("#claveFormularioUsuario").val();


        if(dniUsuario != "" && nombreUsuario != "" && tipoUsuario != "0" && estadoUsuario != "0" && claveUsuario != ""){

            alertify.confirm("¿ SEGURO DE GUARDAR EL REGISTRO ?", function (e) {
                if (e) {

                    var objetoFormulario = new Object()
                        objetoFormulario.procedimiento = procedimiento;
                        objetoFormulario.idUsuario = idUsuario;
                        objetoFormulario.dniUsuario = dniUsuario;
                        objetoFormulario.nombreUsuario = nombreUsuario;
                        objetoFormulario.fechaUsuario = fechaUsuario;
                        objetoFormulario.tipoUsuario = tipoUsuario;
                        objetoFormulario.estadoUsuario = estadoUsuario;
                        objetoFormulario.claveUsuario = claveUsuario;
                        objetoFormulario.usuario = usuario;


                    service.procesar("saveFormularioUsuario",objetoFormulario,cargaFormularioUsuario);

                    alertify.success("HA ACEPTADO GUARDAR EL REGISTRO");

                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });

        }else{
            alertify.error("COMPLETAR INFORMACION DEL FORMULARIO");
        }



    })

    $("#botonCancelarUsuarios").on('click',function(){
        alertify.confirm("¿ SEGURO DE CANCELAR EL PROCEDO ?", function (e) {
            if (e) {
		        alertify.success("PROCESO CANCELADO");
		        formularioUsuarioEstandar();
            }
        });
    })

    $("#botonGuardarEstadoMasivoFormularioUsuario").on('click',function(){
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS MASIVOS ?", function (e) {
            if (e) {

                var objetoFormulario = new Object()
                    objetoFormulario.estado = $("select#estadoMasivoFormularioUsuario").val();
                    objetoFormulario.usuarios = arrayUsuarios;
                    objetoFormulario.usuario = usuario;

                service.procesar("modificarMasivoFormularioUsuario",objetoFormulario,cargaModificarMasivoFormularioUsuario);

                alertify.success("HA ACEPTADO MODIFICAR LOS REGISTROS");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    })

    $("#botonEliminarMasivoFormularioUsuario").on('click',function(){
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS MASIVOS ?", function (e) {
            if (e) {

                var objetoFormulario = new Object()
                    objetoFormulario.usuarios = arrayUsuarios;
                    objetoFormulario.usuario = usuario;

                service.procesar("eliminarMasivoFormularioUsuario",objetoFormulario,cargaEliminarMasivoFormularioUsuario);

                alertify.success("HA ACEPTADO ELIMINAR LOS REGISTROS");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    })

    function cargaEliminarMasivoFormularioUsuario(evt){
    	if(evt == 1 ){
            alertify.success("REGISTROS ELIMINADOS SATISFACTORIAMENTE");
            $(".opciones").css("display","");
        	$(".opcionesmasivo").css("display","none");
        	arrayUsuarios.length=0;
            service.procesar("getListaUsuarios",cargaListaUsuarios);
        }else{
        	alertify.error("PROCESO NO EJECUTADO");
        }
    }

    function cargaModificarMasivoFormularioUsuario(evt){
    	if(evt == 1 ){
            alertify.success("REGISTROS GUARDADOS SATISFACTORIAMENTE");
            $(".opciones").css("display","");
        	$(".opcionesmasivo").css("display","none");
        	arrayUsuarios.length=0;
            service.procesar("getListaUsuarios",cargaListaUsuarios);
        }else{
        	alertify.error("PROCESO NO EJECUTADO");
        }
    }

    function cargaFormularioUsuario(evt){
        if(evt == 1 ){
            alertify.success("REGISTRO GUARDADO SATISFACTORIAMENTE");
            service.procesar("getListaUsuarios",cargaListaUsuarios);
            formularioUsuarioEstandar();
        }else if(evt == 2 ){
            alertify.success("REGISTRO MODIFICADO SATISFACTORIAMENTE");
            service.procesar("getListaUsuarios",cargaListaUsuarios);
            formularioUsuarioEstandar();
        }else{
            alertify.error("REGISTRO NO GUARDADO"); 
        }
    }

    function formularioUsuarioEstandar(){
        $("#dniFormularioUsuario").val("");
        $("#nombreFormularioUsuario").val("");
        $("#claveFormularioUsuario").val("");
        $("#tipoFormularioUsuario").val(0);
        $("#estadoFormularioUsuario").val(1);
        $("#botonGuardarUsuarios").html("GUARDAR");
    }


    $("#botonListarImprimirUsuarios").on('click',function(){
        //service.procesar("imprimirReporteUsuariosActivos",0,mensajeImprimirReporteUsuariosActivos);
        window.open("reportes/reporteUsuariosxEstado.php");
    })

    function mensajeImprimirReporteUsuariosActivos(){
        console.log("impresion enviada");
    }










    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

}


