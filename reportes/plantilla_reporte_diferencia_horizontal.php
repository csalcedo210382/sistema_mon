<?php
	error_reporting(0);
	require_once( "../php/includes/fpdf/fpdf.php" );

	class PDF extends FPDF
	{
		function Header()
		{

		    require_once('../php/config.php');
		    require_once('../php/services/ServiceReportes.php');

		    $servicio = new ServiceReportes();

		    $resultadoTienda = $servicio->getListaReporteTienda();
		    $numeroTienda = $resultadoTienda[0]->numeroTienda;
		    $nombreTienda = $resultadoTienda[0]->nombreTienda;

			$this->Image( "../img/logoReporte.png", 10, 5, 0 );
			$this->SetFont('Arial','',8);
			$this->SetFont('Arial','B',8);
			$this->Cell(20);
			$this->Cell(240, 5, "$nombreTienda - $numeroTienda", 0, 0, 'C' );
			$this->SetFont('Arial','',8);
			$this->Cell(20, 0, date("Y-m-d"), 0, 0, 'R' );
			$this->Ln(3);

			$this->Cell(20);
			$this->Cell(240);
			$this->Cell(20, 0, date("H:i:s"), 0, 0, 'R' );
			$this->Ln(10);


	        $this->SetFont( 'Arial', 'B', $tamanoLetra );
	        $this->Cell( 5, $altoFila, 'N', $borde, 0, $alineacion);
	        $this->Cell( 30, $altoFila, 'SKU', $borde, 0, $alineacion);
	        $this->Cell( 30, $altoFila, 'BARRA', $borde, 0, $alineacion);
	        $this->Cell( 130, $altoFila, 'DESCRIPCION', $borde, 0, $alineacion);
	        $this->Cell( 20, $altoFila, 'STOCK', $borde, 0, $alineacion);
	        $this->Cell( 20, $altoFila, 'CONTEO', $borde, 0, $alineacion);
	        $this->Cell( 20, $altoFila, 'DIF UNIT', $borde, 0, $alineacion);
	        $this->Cell( 20, $altoFila, 'S/. DIF SOL', $borde, 0, 'R');
	        $this->Ln(1);



		}
		
		function Footer()
		{
			$this->SetY(-15);
			$this->SetFont('Arial','', 8);
			$this->Cell(0,10, 'Pagina '.$this->PageNo(),0,0,'C' );
		}

	}
?>