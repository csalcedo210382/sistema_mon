<?php 
    error_reporting(0);
    session_start();
    require_once('php/config.php');
    require_once("php/services/ServiceAdministrativo.php");

    $produccion = PRODUCTION_SERVER;

    $dniUsuario = $_SESSION["dniUsuario"];
    $nombreUsuario = $_SESSION["nombreUsuario"];
    $tipoUsuario = $_SESSION["descripcionTipoUsuario"];
    $estadoUsuario = $_SESSION["estadoUsuario"];
    $permisosUsuario = $_SESSION["permisosUsuario"];

    //CREACION DE CARPETAS DEL SISTEMA
    $listacarpetas = "maestro";
    $carpetas = explode(",",$listacarpetas);
    foreach ($carpetas as $carpeta) {
        $ruta_carpeta = "archivos_sistema/archivos_".$carpeta;
        if (!file_exists($ruta_carpeta)) {
            mkdir($ruta_carpeta, 0777, true);
        }
    }
    //FINAL DE CRACION
    //
 ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SISTEMA INTEGRADO DE INVENTARIO</title>

    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="dist/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core CSS -->

    <!--<link href="dist/css/bootstrap-select.min.css" rel="stylesheet">
    
    <link href="dist/css/bootstrap-table.css" rel="stylesheet">-->



    <!--<link href="dist/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />-->
    <script src="dist/js/jquery.min.js"></script>
    <!--<script src="dist/js/fileinput.min.js" type="text/javascript"></script>-->

    <link href="dist/css/sb-admin-2.css" rel="stylesheet" type="text/css">

    <!-- DataTables CSS 
    <link href="dist/css/dataTables.bootstrap.css" rel="stylesheet">-->

    <!-- DataTables Responsive CSS 
    <link href="dist/css/dataTables.responsive.css" rel="stylesheet">-->

    <!-- ICONO DE PAGINA -->
    <link rel="icon" type="image/png" href="img/favicon.ico" />

    <!-- Custom Fonts -->
    <link href="dist/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="dist/css/kendo.common.min.css" rel="stylesheet"> <!-- CSS PARA QUE FUNCIONE EL GRAFICO DE KENDO-->

    <link rel="stylesheet" href="dist/css/alertify.core.css" />
    <link rel="stylesheet" href="dist/css/alertify.bootstrap.css" id="toggleCSS" />

    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- ESTILO PERSONALIZADO UPC -->
    <link rel="stylesheet" href="dist/css/all.min.css" />

    <link rel="stylesheet" type="text/css" href="dist/css/style_daca.css">

    <link rel="stylesheet" type="text/css" href="dist/css/form.css">


    <?php if($estadoUsuario != 1){ ?>
        
        <script type="text/javascript">

        window.open("login.php?r=1","_top");

        </script>

    <?php } ?>

    <script type="text/javascript">

        var produccion = <?php echo $produccion?1:0 ?>;

    </script>

    <?php 
    if($_SESSION["estadoUsuario"] == 1){ ?>
        <script type="text/javascript">
           sessionStorage.setItem("logeado",'<?=$_SESSION["logeado"]?>');
           sessionStorage.setItem("estadoUsuario",'<?=$_SESSION["estadoUsiario"]?>');
           sessionStorage.setItem("dniUsuario",'<?=$_SESSION["dniUsuario"]?>');
           sessionStorage.setItem("nombreUsuario",'<?=$_SESSION["nombreUsuario"]?>');
           sessionStorage.setItem("permisosUsuario",'<?=$_SESSION["permisosUsuario"]?>');
           sessionStorage.setItem("tipoUsuario",'<?=$_SESSION["tipoUsuario"]?>');
           //window.open("index.php","_top");
        </script>
    <?php }  ?>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php
                include("secciones/menu_principal.php");
            ?>
            <!-- /.navbar-top-links -->




            <!--<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">

                                        <h4>
                                        </h4>

                            </div>
                             /input-group -->
                    <!--    </li>
                    </ul>
                    <ul class="nav" id="lista_opciones">  id="side-menu" -->




               <!--     </ul>
                </div>
                 /.sidebar-collapse -->
            <!--</div>-->
             <!--/.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">


        <!-- Tab panes -->

            <div class="tab-content contenedor">
                    <BR>


                <!--PAGINA PRINCIPAL DE INICIO-->
                <div class="tab-pane active" id="principal">

                <!--<div class="panel panel-default">
                        <div class="panel-heading">
                            INVENTORY GROUP S.A.C.
                        </div>
                        <div class="panel-body titulo">-->
                    <br>
                    <br>
                    <br>
                    <br>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 centro">
                                        SISTEMA MONDELEZ 1.0
                                    </div>
                                </div>
                            </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <!--</div>
                    </div>-->

                </div>


                <?php if($estadoUsuario == 1){

                    for($i=0 ; $i < $registros ; $i++){
                      include("secciones/modulo_".$data[$i]->enlace.".php");
                    }

                }?>




            </div>
            <!-- /#page-wrapper -->

        </div>
    <!-- /#wrapper -->



        <div class="modal fade" id="modalDesconectarse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">SISTEMA INTEGRADO DE INVENTARIO</h4>
              </div>
              <div class="modal-body">
                    ¿SEGURO DE CERRAR SESION?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
                <a href="login.php?r=1" type="button" class="btn btn-primary btn-sm">ACEPTAR</a>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="modal_conflicto_barras" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">CONFLICTO DE BARRAS - LECTURA</h4>
              </div>
              <div class="modal-body">
            
                            <table id="tablaConflictoBarras" width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="10%">#</th>
                                        <th width="20%">AREA CAP</th>
                                        <th width="50%">BARRA CAP</th>
                                        <th width="20%">CANT CAP</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CERRAR</button>
              </div>
            </div>
          </div>
        </div>

        <?php if($estadoUsuario == 1){


            $data = $service->getControles($permisosUsuario);
            $registros = count($data); //[0]->idPerfil;



            for($i=0 ; $i < $registros ; $i++){
               include("ventanas/modal_".$data[$i]->control.".php");
            }

        }?>





    <!-- jQuery -->
    <script src="dist/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript 
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>-->

    
    <script src="dist/js/alertify.min.js"></script>
    <!-- Custom Theme JavaScript 
    <script type="text/javascript" src="dist/js/sb-admin-2.js"></script>-->
    <script type="text/javascript" src="dist/js/moment.js"></script>
    <script type="text/javascript" src="dist/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="dist/js/bootstrap-datetimepicker.es.js" charset="UTF-8"></script>
    <script type="text/javascript" src="dist/js/date.js"></script>

    <script type="text/javascript" src="dist/js/jquery.auto-complete.js"></script>

    <script type="text/javascript" src="js/vendor/S.js"></script>
    <script type="text/javascript" src="js/vendor/service.js"></script>
    <script type="text/javascript" src="js/vendor/jquery.json-2.4.min.js"></script>


    
    <script type="text/javascript" src="js/vendor/kendo/kendo.all.min.js"></script>
    <!--<script type="text/javascript" src="js/vendor/kendo/console.js"></script>-->

    <script type="text/javascript" src="js/main.js"></script>
    
    <script type="text/javascript" src="dist/js/printThis.js"></script>

    <script type="text/javascript" src="js/ControlAdministrativo.js"></script>

    <script type="text/javascript" src="dist/js/shieldui-all.min.js"></script>
    

    <?php if($estadoUsuario == 1){

        $data = $service->getControles($permisosUsuario);
        $registros = count($data); //[0]->idPerfil;

        for($i=0 ; $i < $registros ; $i++){
           echo ('<script type="text/javascript" src="js/Control_'.$data[$i]->control.'.js"></script>');
        }

    }?>

    <!--<script type="text/javascript" src="dist/js/bootstrap-select.min.js"></script>



    <script src="dist/js/jquery.maskMoney.js" type="text/javascript"></script>
-->

</body>

</html>
