<div class="modal fade" tabindex="-1" role="dialog" id="modalNuevoRegistroProduccion">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">FORMULARIO REGISTRO DE PRODUCCION</h4>
            </div>
            <div class="modal-body">
                <div class="list-group">
        
                    <div class="row">   
                        <div class="col-md-1 texto negrita centro">OPERARIO</div>
                        <div class="col-md-2">
                            <select id="idListaOperario" class="form-control input-sm">
                                <option value='0'>---SELECCIONAR---</option>
                            </select>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-1 texto negrita centro">FECHA</div>
                        <div class="col-md-2">
                            <div class='input-group date' id='datetimepickerFechaRegistro'>
                                <input type='text' id="fechaFormularioRegistro" class="form-control input-sm"></input>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">   
                        <div class="col-md-1 texto negrita centro">OPERARIO</div>
                        <div class="col-md-2">
                            <select id="idListaOperario" class="form-control input-sm">
                                <option value='0'>---SELECCIONAR---</option>
                            </select>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-1 texto negrita centro">FECHA</div>
                        <div class="col-md-2">
                            <div class='input-group date' id='datetimepickerFechaRegistro'>
                                <input type='text' id="fechaFormularioRegistro" class="form-control input-sm"></input>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
            <div class="modal-footer">
                <button id="botonModalRegistroProduccion" type="button" class="btn btn-success">Registrar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->