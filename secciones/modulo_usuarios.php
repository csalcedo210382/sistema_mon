                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="usuarios">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            FORMULARIO DE USUARIOS
                        </div>
                        <div class="panel-body">

                           

                                                    <br>
                                                        <div class="row contenedor opciones">
                                                            <div class="col-md-1 texto negrita derecha">DNI</div>
                                                            <div class="col-md-1">
                                                                <input type="number" id="dniFormularioUsuario" placeholder="DNI" class="form-control input-sm"></input>
                                                                <input type="hidden" id="codigoFormularioUsuario"></input>
                                                            </div>
                                                            <div class="col-md-1 texto negrita derecha">USUARIO</div>
                                                            <div class="col-md-3">
                                                                <input id="nombreFormularioUsuario" placeholder="USUARIO" class="form-control input-sm"></input>
                                                            </div>
                                                            <div class="col-md-1 texto negrita derecha">TIPO</div>
                                                            <div class="col-md-2">
                                                                <select id="tipoFormularioUsuario" class="form-control input-sm">
                                                                 <option value=''>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-1"><button id="botonGuardarUsuarios" type="button" class="btn btn-success btn-sm btn-block">GUARDAR</button></div>
                                                            <div class="col-md-1"><button id="botonListarImprimirUsuarios" type="button" class="btn btn-default btn-sm btn-block"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> IMPRIMIR</button></div>

                                                        </div>
                                                        <div class="row contenedor opciones">
                                                            <div class="col-md-1 texto negrita derecha">CLAVE</div>
                                                            <div class="col-md-1">
                                                                <input type="password" id="claveFormularioUsuario" placeholder="PASSWORD" class="form-control input-sm"></input>
                                                            </div>
                                                            <div class="col-md-1 texto negrita derecha">ESTADO</div>
                                                            <div class="col-md-2">
                                                                <select id="estadoFormularioUsuario" class="form-control input-sm">
                                                                 <option value=''>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-1 texto negrita derecha">FECHA</div>
                                                            <div class="col-md-2">
                                                                <div class='input-group date' id='datetimepickerFechaUsuario'>
                                                                    <input type='text' id="fechaFormularioUsuario" class="form-control input-sm"></input>
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-1"><button id="botonCancelarUsuarios" type="button" class="btn btn-danger btn-sm btn-block">CANCELAR</button></div>
                                                            <div class="col-md-1"><button id="botonActualizarUsuarios" type="button" class="btn btn-info btn-sm btn-block">ACTUALIZAR</button></div>
                                                        </div>

                                                        <div class="row contenedor opcionesmasivo" style="display: none;">
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-1 texto negrita derecha">ESTADO</div>
                                                            <div class="col-md-2">
                                                                <select id="estadoMasivoFormularioUsuario" class="form-control input-sm">
                                                                 <option value=''>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2"><button id="botonGuardarEstadoMasivoFormularioUsuario" type="button" class="btn btn-primary btn-sm btn-block">MODIFICAR SELECCIONADOS</button></div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-2"></div>
                                                        </div>

                                                        <div class="row contenedor opcionesmasivo" style="display: none;">
                                                            <div class="col-md-5"></div>
                                                            <div class="col-md-2"><button id="botonEliminarMasivoFormularioUsuario" type="button" class="btn btn-danger btn-sm btn-block">ELIMINAR SELECCIONADOS</button></div>
                                                            <div class="col-md-5"></div>
                                                        </div>

                                                        <br>
                                                        <table id="tablaResultadoFormularioUsuarios" width="100%" class="table table-striped table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th width="5%">#</th>
                                                                    <th width="10%">DNI</th>
                                                                    <th width="25%">COLABORADOR</th>
                                                                    <th width="5%">CLAVE</th>
                                                                    <!--<th width="5%">HUELLA</th>-->
                                                                    <th width="10%">TIPO</th>
                                                                    <th width="10%">ESTADO</th>
                                                                    <th width="10%">FECHA</th>
                                                                    <th width="5%">SELECCIONAR</th>
                                                                    <th width="15%">OPCIONES</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                </tr>                                              
                                                            </tbody>
                                                        </table>

                                                    <br>
                                                    <br>

                                                              



                    </div>
                </div>
            </div>

