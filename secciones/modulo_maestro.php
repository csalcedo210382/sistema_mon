                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="maestro">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            FORMULARIO CARGA DE ARCHIVOS INICIALES XLSX ( MAESTRO, MAESTRO_CIERRE, MAESTRO_POS, MAESTRO_GRS)
                        </div>
                        <div class="panel-body">

                           

                            <br>
                            <div class="row contenedor">
                                <div class="col-md-12">
                                    <input type='file' id="archivoMaestro" style='visibility:hidden; height:0'>
                                    <div class="form-group">
                                        <div class="input-group" name="Fichero">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary boton-carga-maestro btn-sm" type="button">BUSCAR ARCHIVO</button>
                                            </span>
                                            <input type="text" class="form-control campo-carga-maestro input-sm" placeholder='NOMBRE ARCHIVO MAESTRO...' />
                                            <span class="input-group-btn">
                                                <button class="btn btn-danger boton-limpia-maestro btn-sm" type="button">RESETEAR</button>
                                                <button class="btn btn-success submit-carga-maestro btn-sm" type="button" disabled>CARGAR ARCHIVO</button>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row contenedor progresoCargandoMaestro" style="display:none">
                                <div class="col-md-12">
                                    <div class="progress">
                                      <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">CARGANDO REGISTROS A LA BASE DE DATOS</div>
                                    </div>                                
                                </div>
                            </div>

                            <table id="tablaListaArchivosMaestroPendientes" width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="55%">ARCHIVO</th>
                                        <th width="10%">FECHA</th>
                                        <th width="10%">TAMAÑO</th>
                                        <th width="20%">OPCIONES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    </tr>                                              
                                </tbody>
                            </table>

                            <br>                                                              



                    </div>
                </div>
            </div>

