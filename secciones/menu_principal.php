            <div class="navbar-header">

                <a class="navbar-brand sistema" href="index.php">SISTEMA MONDELEZ 1.0</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                 
                <?php  $service = new ServiceAdministrativo();
                    $barras = $service->conflictoBarras();
                    if($barras > 0){
                        echo '<a id="listarConflictoBarras" href="#modal_conflicto_barras" type="button" data-toggle="modal" class="btn btn-warning btn-sm conflictobarras parpadea">EXISTEN BARRAS NO REGISTRADAS</a> ';
                    }

                    $archivos = $service->archivosPendientes();
                    if($archivos > 0){
                        echo ' <a id="listarArchivosPendientes" href="#archivos_pendientes" type="button" class="btn btn-danger btn-sm" data-toggle="tab">( '.$archivos.' ) ARCHIVOS PENDIENTES DE CARGA</a>';
                    }
                ?>
                
                

                <label id="usuario"></label> [ <span class="cargo" id="tipo"></span> ] 
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        MODULOS DE USUARIO <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!--<li><a href="#"><i class="fa fa-user fa-fw"></i> Perfil de usuario</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Configuracion</a>
                        </li>
                        <li class="divider"></li>-->

                        <?php if($estadoUsuario == 1){

                       
                        $data = $service->getMenu($permisosUsuario);
                        $registros = count($data);

                        for($i=0 ; $i < $registros ; $i++){
                            echo '<li class="menu"><a id="menu_'.$data[$i]->enlace.'" href="#'.$data[$i]->enlace.'" data-toggle="tab">'.$data[$i]->titulo.' </a></li>'; //<i class="fa fa-list-alt fa-fw"></i>
                         }

                        }?>
    
                        <li class="menu"><a href="#modalDesconectarse" class="dropdown-toggle" data-toggle="modal">SALIR</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                
                <!-- /.dropdown -->
            </ul>