$(function () {
    $('#datetimepickerFechaUsuario').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false,
        closeOnDateSelect: true
    });
    
    $('#datetimepickerFechaInventario').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false,
        closeOnDateSelect: true
    });


});